#include <Arduino.h>

#include <Wire.h>
#include <NewPing.h>

// Sensor US #1
#define echo_1 2
#define trig_1 3

// Sensor US #2
#define echo_2 4
#define trig_2 5

// Sensor US #3
#define echo_3 6
#define trig_3 7

// Sensor US #4
#define echo_4 8
#define trig_4 9

// Sensor US #5
#define echo_5 14
#define trig_5 15

// Sensor US #6
#define echo_6 16
#define trig_6 17

// Sensor US #7
#define echo_7 20
#define trig_7 21

// Sensor US #8
#define echo_8 22
#define trig_8 23

#define MAX_DISTANCE 250
//#############################################

//#############################################

// Direccion I2C
#define ADRR 8
//#############################################

// Vector en bytes de salida de datos
byte a[8];
//#############################################

// Variables del encoder y las interrupciones
unsigned long t_anterior = 0;
//#############################################

// Variables de activación
byte data_in[2];

//#############################################

// Ajuste millis()
unsigned int lc_1 = 1;
unsigned long actual = 0;
//#############################################

// Funcion NewPing para los 8 sensores US
NewPing sonar0(trig_1, echo_1, MAX_DISTANCE); // NewPing setup of pin and maximum distance.
NewPing sonar1(trig_2, echo_2, MAX_DISTANCE);
NewPing sonar2(trig_3, echo_3, MAX_DISTANCE);
NewPing sonar3(trig_4, echo_4, MAX_DISTANCE);
NewPing sonar4(trig_5, echo_5, MAX_DISTANCE);
NewPing sonar5(trig_6, echo_6, MAX_DISTANCE);
NewPing sonar6(trig_7, echo_7, MAX_DISTANCE);
NewPing sonar7(trig_8, echo_8, MAX_DISTANCE);
//#############################################

void receiveEvent(int num_bytes)
{
  for (int i = 0; i < num_bytes; i++)
  {
    data_in[i] = Wire.read();
  }
}

void requestEvent()
{
  Wire.write(a, 8); // Enviamos 13 bytes de datos
  // a[12] = millis() - t_anterior;
  // t_anterior = millis();
  //  as expected by master
}

void setup()
{
  Wire.begin(ADRR); // join i2c bus with address #4
  Wire.onRequest(requestEvent);
  Wire.onReceive(receiveEvent);
  Serial.begin(9600); // start serial for output
}

void loop()
{
  //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
  if (data_in[0] == 1)
  {
    if (lc_1 == 1)
    {
      a[0] = sonar0.ping_cm();

      a[2] = sonar2.ping_cm();

      a[4] = sonar4.ping_cm();

      a[6] = sonar6.ping_cm();

      actual = millis();
      lc_1 = 0;
    }

    if (millis() > actual + 10)
    {
      a[1] = sonar1.ping_cm();

      a[3] = sonar3.ping_cm();

      a[5] = sonar5.ping_cm();

      a[7] = sonar7.ping_cm();

      lc_1 = 1;
    }
  }
}
