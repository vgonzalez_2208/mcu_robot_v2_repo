#include <Arduino.h>

// Puertos de PWM
#define pwm_1a 7 // Rueda izquierda
#define pwm_1b 8 // Rueda izquierda

// Puertos de entrada de los encoder
#define ENC_1 1
#define ENC_2 0

byte flag_ini = 0;
unsigned long inicio = 0;
unsigned int contador = 0;

void enc1()
{
  contador++;
}

void setup()
{
  attachInterrupt(digitalPinToInterrupt(ENC_1), enc1, CHANGE);
  analogReadResolution(10);
  pinMode(pwm_1a, OUTPUT);
  pinMode(pwm_1b, OUTPUT);
}

void loop()
{
  if (millis() > 5000 && flag_ini == 0)
  {
    analogWrite(pwm_1a, 1023);
    analogWrite(pwm_1b, 0);
    flag_ini = 1;
    inicio = millis();
    contador = 0;
  }
  else{
    if (contador == 1 && flag_ini == 1)
    {
      unsigned int retardo = 0;
      retardo = millis() - inicio;
      flag_ini = 2;
      Serial.println(retardo);
      analogWrite(pwm_1a, 0);
    }
  }
}