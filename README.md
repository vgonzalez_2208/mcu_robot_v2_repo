# MCU_Robot_V2_repo
### Forma parte de trabajo final de grado.

Repositorio de código creado para los microcontroladores utilizados en el Robot V2.

## Detalles iniciales

El código aquí mostrado corresponde a los diferentes algoritmos escritos en C++ utilizando la librería Arduino bajo PlatformIO como IDE.

Los microcontroladores utilizados para este Robot V2 fueron:
- Seeeduino XIAO AtmelSamd21
- PJRC Teensy 3.2

Las carpetas mostradas cotienen el código de las siguientes aplicaciones:

- Control PID de Robot V1: Creada para el Seeeduino XIAO, carpeta "PID_robot_control_v1", permite configurar este microcontrolador como esclavo I²C para ejecutar diversas tareas según determine el maestro del bus. Básicamente permite enviar al maestro del bus información de tiempo en milisegundos entre pulsos de los encoders de velocidad disponibles en el Robot para de esta forma modelar una FDT lo más cercana al comportamiento del sistema.

- Medición de retardo de motor DC: Creada para el Seeeduino XIAO, carpeta "retardo_motor_escalon", permite aplicar un pulso escalón de 5 voltios y medir el tiempo entre la aplicación del pulso y el inicio del movimiento del rotor. Este tiempo transcurrido también se conoce como tiempo muerto.

- Información de sensores Ultrasónicos: Creada para el PJRC Teensy 3.2, carpeta "teensy_us_i2c_v3", permite configurar este microcontrolador como esclavo y recopilar la información de los 8 sensores Ultrasónicos del Robot en un vector de 8 posiciones para enviarlo al maestro del bus cuando este lo solicite.

La documentación del codigo se agregará a medida que surjan dudas de las diversas secciones del proyecto.
