#include <i2c_mod_test/i2c_mod_test.h>

void check_status(byte (&m_1)[4], byte (&m_2)[4], byte (&b)[1], u_long start_time, u_long test_time)
{
  if (m_1[0] != 0 and m_2[0] != 0)
  {
    b[0] = 3;
  }
  else if (m_1[0] == 0 and m_2[0] == 0)
  {
    b[0] = 0;
  }
  else if (m_1[0] != 0 and m_2[0] == 0)
  {
    b[0] = 1;
  }
  else if (m_1[0] == 0 and m_2[0] != 0)
  {
    b[0] = 2;
  }
  if (millis() >= (start_time + test_time))
  {
    b[0] = 4;
  }
}

void clean_value(byte (&val)[4])
{
  val[0] = 0;
  val[1] = 0;
  val[2] = 0;
  // val[4] = 0;
}

void send_info(byte (&a)[16], byte b[1], byte (&m_1)[4], byte (&m_2)[4])
{
  if (a[0] == 0)
  {
    Wire.write(b, 1);
    if (b[0] != 0)
    {
      a[0] = 1;
    }
  }
  else if (a[0] == 1)
  {
    if (b[0] == 3)
    {
      byte m_all[10] = {m_1[0], m_1[1], m_1[2], m_1[3], m_2[0], m_2[1], m_2[2], m_2[3]};
      // Wire.write(m_1, 5);
      Wire.write(m_all, 8);
      clean_value(m_1);
      clean_value(m_2);
    }
    else if (b[0] == 1)
    {
      Wire.write(m_1, 4);
      clean_value(m_1);
    }
    else if (b[0] == 2)
    {
      Wire.write(m_2, 4);
      clean_value(m_2);
    }
  }
}

void reset_values(u_long &start_time, byte (&data_ent)[16])
{
  start_time = 0;
  for (int i = 0; i < 16; i++)
  {
    data_ent[i] = 0;
  }
}

void check_mov(byte (&data_ent)[16], u_int &test_time, u_int &pwm_val, u_long &start_time, byte &flag_ini, byte &test_type, bool &flag_rst_cont)
{
  start_time = millis() + 10;
  test_time = (data_ent[3] << 8) | data_ent[4];
  pwm_val = (1024 * data_ent[2]) / 100 - 1;
  // if (data_ent[1] == 5)
  // {
  //   flag_ini = 0;
  //   reset_values(start_time, data_ent);
  //   flag_rst_cont = true;
  // }
  // else
  // {
  flag_ini = 1;
  // }
  test_type = data_ent[1];
  data_ent[1] = 0; // Test en ejecución.
}

void reset_contadores(vol_u_int &cont_enc1, vol_u_int &cont_enc2, vol_u_int &cont_ant1, vol_u_int &cont_ant2, u_long &millis_enc1, u_long &millis_enc2)
{
  cont_enc1 = 0;
  cont_enc2 = 0;
  cont_ant1 = 0;
  cont_ant2 = 0;
  millis_enc1 = 0;
  millis_enc2 = 0;
}