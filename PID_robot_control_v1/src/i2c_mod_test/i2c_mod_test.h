#include <Arduino.h>
#include <Wire.h>

typedef unsigned int u_int;
typedef unsigned long u_long;
typedef volatile unsigned int vol_u_int;

void check_status(byte (&m_1)[4], byte (&m_2)[4], byte (&b)[1], u_long start_time, u_long test_time);

void clean_value(byte (&val)[5]);

void send_info(byte (&data_ent)[16], byte b[1], byte (&m_1)[4], byte (&m_2)[4]);

void reset_values(u_long &start_time, byte (&data_ent)[16]);

void check_mov(byte (&data_ent)[16], u_int &test_time, u_int &pwm_val, u_long &start_time, byte &flag_ini, byte &test_type, bool &flag_rst_cont);

void reset_contadores(vol_u_int &cont_enc1, vol_u_int &cont_enc2, vol_u_int &cont_ant1, vol_u_int &cont_ant2, u_long &millis_enc1, u_long &millis_enc2);
