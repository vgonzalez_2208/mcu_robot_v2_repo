#include <Arduino.h>

typedef unsigned long u_long;

struct k_values
{
  double kp, ki, kd;
};

struct motor
{
  k_values m_a, m_b;
};

struct k_struct
{
  motor dir_avance, dir_retroceso, dir_giro_izq, dir_giro_der;
  byte l_pwm;
};

void save_k_info(byte (&data)[73], k_struct &k_info);

void set_control_remoto(byte (&data_ent)[16], byte &flag_ini, byte (&dir_control)[3]);

void mov_control_pid(byte m1_a, byte m1_b, byte m2_a, byte m2_b, byte dir, double pwm_ma, double pwm_mb);

void mov_step_control(byte (&data_ent)[16], byte &flag_ini, u_long &millis_step_ini, byte (&step_info)[6]);