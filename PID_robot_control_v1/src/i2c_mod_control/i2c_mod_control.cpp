#include <i2c_mod_control/i2c_mod_control.h>

void save_k_info(byte (&data)[73], k_struct &k_info)
{
    byte n = 0;
    // Dir. de avance
    k_info.dir_avance.m_a.kp = (float)((data[n + 1] << 8) | data[n + 2]) / 10000.0 + (float)data[n];
    n = n + 3;
    k_info.dir_avance.m_a.ki = (float)((data[n + 1] << 8) | data[n + 2]) / 10000.0 + (float)data[n];
    n = n + 3;
    k_info.dir_avance.m_a.kd = (float)((data[n + 1] << 8) | data[n + 2]) / 10000.0 + (float)data[n];
    n = n + 3;
    k_info.dir_avance.m_b.kp = (float)((data[n + 1] << 8) | data[n + 2]) / 10000.0 + (float)data[n];
    n = n + 3;
    k_info.dir_avance.m_b.ki = (float)((data[n + 1] << 8) | data[n + 2]) / 10000.0 + (float)data[n];
    n = n + 3;
    k_info.dir_avance.m_b.kd = (float)((data[n + 1] << 8) | data[n + 2]) / 10000.0 + (float)data[n];
    n = n + 3;
    // Dir. retroceso
    k_info.dir_retroceso.m_a.kp = (float)((data[n + 1] << 8) | data[n + 2]) / 10000.0 + (float)data[n];
    n = n + 3;
    k_info.dir_retroceso.m_a.ki = (float)((data[n + 1] << 8) | data[n + 2]) / 10000.0 + (float)data[n];
    n = n + 3;
    k_info.dir_retroceso.m_a.kd = (float)((data[n + 1] << 8) | data[n + 2]) / 10000.0 + (float)data[n];
    n = n + 3;
    k_info.dir_retroceso.m_b.kp = (float)((data[n + 1] << 8) | data[n + 2]) / 10000.0 + (float)data[n];
    n = n + 3;
    k_info.dir_retroceso.m_b.ki = (float)((data[n + 1] << 8) | data[n + 2]) / 10000.0 + (float)data[n];
    n = n + 3;
    k_info.dir_retroceso.m_b.kd = (float)((data[n + 1] << 8) | data[n + 2]) / 10000.0 + (float)data[n];
    n = n + 3;
    // Dir. de giro izq.
    k_info.dir_giro_izq.m_a.kp = (float)((data[n + 1] << 8) | data[n + 2]) / 10000.0 + (float)data[n];
    n = n + 3;
    k_info.dir_giro_izq.m_a.ki = (float)((data[n + 1] << 8) | data[n + 2]) / 10000.0 + (float)data[n];
    n = n + 3;
    k_info.dir_giro_izq.m_a.kd = (float)((data[n + 1] << 8) | data[n + 2]) / 10000.0 + (float)data[n];
    n = n + 3;
    k_info.dir_giro_izq.m_b.kp = (float)((data[n + 1] << 8) | data[n + 2]) / 10000.0 + (float)data[n];
    n = n + 3;
    k_info.dir_giro_izq.m_b.ki = (float)((data[n + 1] << 8) | data[n + 2]) / 10000.0 + (float)data[n];
    n = n + 3;
    k_info.dir_giro_izq.m_b.kd = (float)((data[n + 1] << 8) | data[n + 2]) / 10000.0 + (float)data[n];
    n = n + 3;
    // Dir. de giro der.
    k_info.dir_giro_der.m_a.kp = (float)((data[n + 1] << 8) | data[n + 2]) / 10000.0 + (float)data[n];
    n = n + 3;
    k_info.dir_giro_der.m_a.ki = (float)((data[n + 1] << 8) | data[n + 2]) / 10000.0 + (float)data[n];
    n = n + 3;
    k_info.dir_giro_der.m_a.kd = (float)((data[n + 1] << 8) | data[n + 2]) / 10000.0 + (float)data[n];
    n = n + 3;
    k_info.dir_giro_der.m_b.kp = (float)((data[n + 1] << 8) | data[n + 2]) / 10000.0 + (float)data[n];
    n = n + 3;
    k_info.dir_giro_der.m_b.ki = (float)((data[n + 1] << 8) | data[n + 2]) / 10000.0 + (float)data[n];
    n = n + 3;
    k_info.dir_giro_der.m_b.kd = (float)((data[n + 1] << 8) | data[n + 2]) / 10000.0 + (float)data[n];
    // Nivel de PWM
    k_info.l_pwm = data[n + 3];
}

void set_control_remoto(byte (&data_ent)[16], byte &flag_ini, byte (&dir_control)[3])
{
    // for (byte i = 0; i < 3; i++)
    // {
    //     Serial.println(data_ent[i]);
    // }
    if (data_ent[1] == 5)
    {
        flag_ini = 0;
    }
    else
    {
        flag_ini = 2;
        if (dir_control[0] == data_ent[1])
        {
            dir_control[2] = 5; // Flag de aplicar ajuste; 1 si es cambio de dirección y 5 si es cambio de velocidad.
        }
        else
        {
            dir_control[2] = 1; // Flag de aplicar ajuste; 1 si es cambio de dirección y 5 si es cambio de velocidad.
        }
        dir_control[0] = data_ent[1]; // Dirección de avance
        dir_control[1] = data_ent[2]; // Setpoint de velocidad
        // start_push = 1;
    }
}

void mov_control_pid(byte m1_a, byte m1_b, byte m2_a, byte m2_b, byte dir, double pwm_m1, double pwm_m2)
{
    Serial.println(m1_a);
    Serial.println(m1_b);
    Serial.println(pwm_m1);
    Serial.println(pwm_m2);
    // Rueda izquierda
    if (dir == 1 || dir == 4)
    {
        analogWrite(m1_a, 0);      // Movimiento hacia atrás HIGH
        analogWrite(m1_b, pwm_m1); // Movimiento hacia adelante HIGH
    }
    else
    {
        analogWrite(m1_a, pwm_m1); // Movimiento hacia atrás HIGH
        analogWrite(m1_b, 0);      // Movimiento hacia adelante HIGH
    }
    // Rueda derecha
    if (dir == 1 || dir == 3)
    {
        analogWrite(m2_a, pwm_m2); // Movimiento hacia adelante HIGH
        analogWrite(m2_b, 0);      // Movimiento hacia atrás HIGH
    }
    else
    {
        analogWrite(m2_a, 0);      // Movimiento hacia adelante HIGH
        analogWrite(m2_b, pwm_m2); // Movimiento hacia atrás HIGH
    }
}

void mov_step_control(byte (&data_ent)[16], byte &flag_ini, u_long &millis_step_ini, byte (&step_info)[6])
{
    step_info[0] = 1; // Flag de inicio de steps
    step_info[1] = data_ent[1]; // Cantidad de pasos
    step_info[2] = data_ent[2]; // Tiempo de pasos
    step_info[3] = data_ent[3]; // Tiempo entre pasos
    step_info[4] = data_ent[4]; // Dirección de pasos
    step_info[5] = 0; // Reset de contador de pasos
    millis_step_ini = millis(); // Millis de inicio de pasos
    flag_ini = 6; // Flag de inicio en void loop
}