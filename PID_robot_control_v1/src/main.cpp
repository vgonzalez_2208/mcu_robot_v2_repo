#include <functions/functions.h>
#include <i2c_mod_test/i2c_mod_test.h>
#include <i2c_mod_control/i2c_mod_control.h>
#include <PID_v1.h>

// Variables contadoras de pulsos
vol_u_int cont_enc1 = 0, cont_ant1 = 0;
vol_u_int cont_enc2 = 0, cont_ant2 = 0;
bool flag_rst_cont = false;

// Variables para verificar el tiempo mínimo entre pulsos
u_long millis_enc1 = 0, millis_enc2 = 0;
u_long delta_t1 = 0, delta_t2 = 0, start_time = 0;
// Variable de inicio movimiento
u_long start_push = 0;
byte fl_stb[2] = {0, 0};

// Variable que determina tipo de prueba. Posición 1: rueda izq. o der.; Posición 2: dirección.
// byte tipo_prueba[] = {1, 1};
byte test_type = 0;
u_int pwm_val = 0;
u_int test_time = 0;
byte flag_ini = 0;

// Variables para comunicación i2c
byte data_ent[16], b[1]; // Data_ent: [0] info de envío; [1] tipo prueba; [2] nivel pwm; [3][4] tiempo de prueba en ms.
byte m_1[4] = {0, 0, 0, 1};
byte m_2[4] = {0, 0, 0, 2};

// ### Variables para control PID ###
// Variables conectadas
double Setpoint, Input_1, Output_1, Input_2, Output_2;

// Arreglo para guardar ajustes de PID
byte k_data[73]; // Pendiente de ajuste
double k_data_conv[25];
double act_kp[6]; // Parámetro de Kp, Ki y Kd activos para las dos ruedas.
byte tune_pid[4];
u_long millis_tune = 0;
byte step_info[6];          // [0]: flag_salida | [1]: pasos | [2]: tiempo pasos | [3]: tiempo entre pasos | [4]: dirección | [5]: conta pasos (tiempo x 10)
u_long millis_step_ini = 0; // Tiempo entre pulsos para ajuste de posición

// Parámetros iniciales de PID
// double Kp = 2.2696, Ki = 8.4206, Kd = 0.11938;
double Kp = 0, Ki = 0, Kd = 0;
PID rueda_1(&Input_1, &Output_1, &Setpoint, Kp, Ki, Kd, DIRECT);
PID rueda_2(&Input_2, &Output_2, &Setpoint, Kp, Ki, Kd, DIRECT);

// Estructura de parámetros de PID
struct k_struct k_info;

byte dir_control[3]; // Pos. 0: dirección; pos. 1: setpoint velocidad; pos. 2: flag set.

void reset_all()
{
  b[0] = 0;
  cont_enc1 = 0;
  cont_enc2 = 0;
  cont_ant1 = 0;
  cont_ant2 = 0;
  millis_enc1 = 0;
  millis_enc2 = 0;
  start_time = 0;
  flag_ini = 0;
  test_time = 0;
  delta_t1 = 0;
  delta_t2 = 0;
  test_type = 0;
  pwm_val = 0;
  m_1[0] = 0;
  m_1[1] = 0;
  m_1[2] = 0;
  m_2[0] = 0;
  m_2[1] = 0;
  m_2[2] = 0;
}

void receiveEvent(int num_bytes)
{
  if (num_bytes > 5)
  {
    for (int i = 0; i < num_bytes; i++)
    {
      k_data[i] = Wire.read();
    }
    save_k_info(k_data, k_info); // Guardado de parámetros de K en struct
  }
  else
  {
    for (int i = 0; i < num_bytes; i++)
    {
      data_ent[i] = Wire.read();
    }
    if (data_ent[0] <= 1)
    {
      if (0 < data_ent[1] && data_ent[1] < 5)
      {
        reset_all();
        check_mov(data_ent, test_time, pwm_val, start_time, flag_ini, test_type, flag_rst_cont);
        // reset_contadores(cont_enc1, cont_enc2, cont_ant1, cont_ant2, millis_enc1, millis_enc2);
      }
      else if (data_ent[1] == 5)
      {
        stop_mov();
      }
    }
    else if (data_ent[0] == 2) // Set inicio de control remoto
    {
      set_control_remoto(data_ent, flag_ini, dir_control);
    }
    else if (data_ent[0] == 3) // Set inicio de ajuste de salida PID
    {
      tune_pid[0] = 1;
      flag_ini = 5;
    }
    else if (data_ent[0] == 4) // Set de inicio de pasos @1023 pwm.
    {
      mov_step_control(data_ent, flag_ini, millis_step_ini, step_info);
    } 
  }
}

void requestEvent() // Para a = 0 se envía estatus de lecturas; para a = 1 se envían lecturas.
{
  if (data_ent[0] == 0)
  {
    check_status(m_1, m_2, b, start_time, test_time);
    send_info(data_ent, b, m_1, m_2);
  }
  else if (data_ent[0] == 1)
  {
    send_info(data_ent, b, m_1, m_2);
  }
  else if (data_ent[0] == 2)
  {
    /* Enviar info de movimiento */
  }
}

void enc1()
{
  if (millis() - millis_enc1 > 15)
  {
    cont_enc1++;
  }
}

void enc2()
{
  if (millis() - millis_enc2 > 15)
  {
    cont_enc2++;
  }
}

void setup()
{
  Serial.begin(9600);
  // Interrupciones de los encoder de velocidad
  attachInterrupt(digitalPinToInterrupt(ENC_1), enc1, FALLING);
  attachInterrupt(digitalPinToInterrupt(ENC_2), enc2, FALLING);
  // Info de bus i2c
  Wire.begin(10);               // join i2c bus with address #10
  Wire.onReceive(receiveEvent); // register event
  Wire.onRequest(requestEvent);
  pinMode(pwm_1a, OUTPUT);
  pinMode(pwm_1b, OUTPUT);
  pinMode(pwm_2a, OUTPUT);
  pinMode(pwm_2b, OUTPUT);
  analogWriteResolution(10);
  Setpoint = 1 / 0.08;
  rueda_1.SetOutputLimits(100, 1023);
  rueda_2.SetOutputLimits(100, 1023);
  // Iniciar PID
  rueda_1.SetMode(AUTOMATIC);
  rueda_2.SetMode(AUTOMATIC);
}

void loop()
{
  if (flag_ini == 1) // Modo test activo
  {
    if (start_time < millis() && millis() <= (start_time + test_time))
    {
      inicio_mov(test_type, pwm_val);
      // Info de rueda izquierda
      gen_info_test(cont_enc1, cont_ant1, millis_enc1, delta_t1, start_time, m_1);
      // Info de rueda derecha
      gen_info_test(cont_enc2, cont_ant2, millis_enc2, delta_t2, start_time, m_2);
    }
    else
    {
      stop_mov();
      if (b[0] == 4)
      {
        reset_all();
      }
    }
  }
  else if (flag_ini == 2) // Modo desplazamiento activo
  {
    // Serial.println("Inicio movimiento");
    if (dir_control[2] == 1 || dir_control[2] == 5) // Si es 5 significa cambio de velocidad en la misma dirección.
    {
      if (dir_control[0] == 1)
      {
        rueda_1.SetTunings(k_info.dir_avance.m_a.kp, k_info.dir_avance.m_a.ki, k_info.dir_avance.m_a.kd);
        rueda_2.SetTunings(k_info.dir_avance.m_b.kp, k_info.dir_avance.m_b.ki, k_info.dir_avance.m_b.kd);
      }
      else if (dir_control[0] == 2)
      {
        rueda_1.SetTunings(k_info.dir_retroceso.m_a.kp, k_info.dir_retroceso.m_a.ki, k_info.dir_retroceso.m_a.kd);
        rueda_2.SetTunings(k_info.dir_retroceso.m_b.kp, k_info.dir_retroceso.m_b.ki, k_info.dir_retroceso.m_b.kd);
      }
      else if (dir_control[0] == 3)
      {
        rueda_1.SetTunings(k_info.dir_giro_izq.m_a.kp, k_info.dir_giro_izq.m_a.ki, k_info.dir_giro_izq.m_a.kd);
        rueda_2.SetTunings(k_info.dir_giro_izq.m_b.kp, k_info.dir_giro_izq.m_b.ki, k_info.dir_giro_izq.m_b.kd);
      }
      else if (dir_control[0] == 4)
      {
        rueda_1.SetTunings(k_info.dir_giro_der.m_a.kp, k_info.dir_giro_der.m_a.ki, k_info.dir_giro_der.m_a.kd);
        rueda_2.SetTunings(k_info.dir_giro_der.m_b.kp, k_info.dir_giro_der.m_b.ki, k_info.dir_giro_der.m_b.kd);
      }
      Setpoint = 1000 / dir_control[1];
      if (dir_control[2] == 5)
      {
        dir_control[2] = 4;
      }
      else
      {
        dir_control[2] = 2;
      }
    }
    if (dir_control[2] == 2)
    {
      Input_1 = 0;
      Input_2 = 0;
      cont_ant1 = cont_enc1;
      cont_ant2 = cont_enc2;
      millis_enc1 = millis();
      millis_enc2 = millis();
      fl_stb[0] = 0;
      fl_stb[1] = 0;
      // Impulso inicial hasta que suceda el primer pulso
      // Output_1 = 490;
      // Output_2 = 490;
      mov_control_pid(pwm_1a, pwm_1b, pwm_2a, pwm_2b, dir_control[0], Output_1, Output_2);
      dir_control[2] = 4;
      // Temporal
      fl_stb[0] = 1;
      fl_stb[1] = 1;
    }
    else if (dir_control[2] == 3)
    {
      if (cont_enc1 - cont_ant1 >= 1 && fl_stb[0] == 0)
      {
        fl_stb[0] = 1;
        cont_ant1 = cont_enc1;
        millis_enc1 = millis();
      }
      else
      {
        Input_1 = 0;
      }
      if (cont_enc2 - cont_ant2 >= 1 && fl_stb[1] == 0)
      {
        fl_stb[1] = 1;
        cont_ant2 = cont_enc2;
        millis_enc2 = millis();
      }
      else
      {
        Input_2 = 0;
      }
      if (fl_stb[0] == 1 && fl_stb[1] == 1)
      {
        dir_control[2] = 4;
      }
    }
    if (cont_ant1 != cont_enc1 && fl_stb[0] == 1)
    {
      Input_1 = 1000 / ((millis() - millis_enc1) / (cont_enc1 - cont_ant1));
      cont_ant1 = cont_enc1;
      millis_enc1 = millis();
    }
    else if (cont_ant1 == 0 && fl_stb[0] == 1)
    {
      Input_1 = 0;
    }
    if (cont_ant2 != cont_enc2 && fl_stb[1] == 1)
    {
      Input_2 = 1000 / ((millis() - millis_enc2) / (cont_enc2 - cont_ant2));
      cont_ant2 = cont_enc2;
      millis_enc2 = millis();
    }
    else if (cont_ant2 == 0 && fl_stb[1] == 1)
    {
      Input_2 = 0;
    }
    rueda_1.Compute();
    rueda_2.Compute();
    if (dir_control[2] == 4)
    {
      mov_control_pid(pwm_1a, pwm_1b, pwm_2a, pwm_2b, dir_control[0], Output_1, Output_2);
    }
  }

  else if (flag_ini == 5) // Ajuste de salida PID
  {
    if (tune_pid[0] == 1)
    {
      rueda_1.SetTunings(k_info.dir_avance.m_a.kp, k_info.dir_avance.m_a.ki, k_info.dir_avance.m_a.kd);
      rueda_2.SetTunings(k_info.dir_avance.m_b.kp, k_info.dir_avance.m_b.ki, k_info.dir_avance.m_b.kd);
      Setpoint = 1000 / 30;
      Input_1 = 0;
      Input_2 = 0;
      cont_ant1 = cont_enc1;
      cont_ant2 = cont_enc2;
      millis_enc1 = millis();
      millis_enc2 = millis();
      mov_control_pid(pwm_1a, pwm_1b, pwm_2a, pwm_2b, 1, 490, 490);
      tune_pid[1] = 0;
      tune_pid[2] = 0;
      tune_pid[0] = 2;
    }
    else if (tune_pid[0] == 2)
    {
      if (cont_enc1 > cont_ant1 && tune_pid[1] <= 15)
      {
        Input_1 = 1000 / ((millis() - millis_enc1) / (cont_enc1 - cont_ant1));
        cont_ant1 = cont_enc1;
        millis_enc1 = millis();
        ++tune_pid[1];
      }
      if (cont_enc2 > cont_ant2 && tune_pid[2] <= 15)
      {
        Input_2 = 1000 / ((millis() - millis_enc2) / (cont_enc2 - cont_ant2));
        cont_ant2 = cont_enc2;
        millis_enc2 = millis();
        ++tune_pid[2];
      }
      rueda_1.Compute();
      rueda_2.Compute();
      mov_control_pid(pwm_1a, pwm_1b, pwm_2a, pwm_2b, 1, Output_1, Output_2);
      if (tune_pid[1] == 16 && tune_pid[2] == 16)
      {
        tune_pid[0] = 3;
        millis_tune = millis();
      }
    }
    else if (tune_pid[0] == 3)
    {
      if (millis() < millis_tune + 500)
      {
        mov_control_pid(pwm_1a, pwm_1b, pwm_2a, pwm_2b, 2, 0, 0);
      }
      else
      {
        rueda_1.SetTunings(k_info.dir_retroceso.m_a.kp, k_info.dir_retroceso.m_a.ki, k_info.dir_retroceso.m_a.kd);
        rueda_2.SetTunings(k_info.dir_retroceso.m_b.kp, k_info.dir_retroceso.m_b.ki, k_info.dir_retroceso.m_b.kd);
        Setpoint = 1000 / 80; // Setpint final
        Input_1 = 0;
        Input_2 = 0;
        cont_ant1 = cont_enc1;
        cont_ant2 = cont_enc2;
        millis_enc1 = millis();
        millis_enc2 = millis();
        mov_control_pid(pwm_1a, pwm_1b, pwm_2a, pwm_2b, 2, 490, 490);
        tune_pid[1] = 0;
        tune_pid[2] = 0;
        tune_pid[0] = 4;
      }
    }
    else if (tune_pid[0] == 4)
    {
      if (cont_enc1 > cont_ant1 && tune_pid[1] <= 15)
      {
        Input_1 = 1000 / ((millis() - millis_enc1) / (cont_enc1 - cont_ant1));
        cont_ant1 = cont_enc1;
        millis_enc1 = millis();
        ++tune_pid[1];
      }
      if (cont_enc2 > cont_ant2 && tune_pid[2] <= 15)
      {
        Input_2 = 1000 / ((millis() - millis_enc2) / (cont_enc2 - cont_ant2));
        cont_ant2 = cont_enc2;
        millis_enc2 = millis();
        ++tune_pid[2];
      }
      rueda_1.Compute();
      rueda_2.Compute();
      mov_control_pid(pwm_1a, pwm_1b, pwm_2a, pwm_2b, 2, Output_1, Output_2);
      if (tune_pid[1] == 16 && tune_pid[2] == 16)
      {
        tune_pid[0] = 0; // Salida del pid_tune loop
        // millis_tune = millis();
        mov_control_pid(pwm_1a, pwm_1b, pwm_2a, pwm_2b, 2, 0, 0);
      }
    }
  }
  else if (flag_ini == 6)
  {
    if (step_info[5] < step_info[1])
    {
      if (step_info[0] == 1)
      {
        if (millis() < (millis_step_ini + (u_long)step_info[2] * 10))
        {
          mov_control_pid(pwm_1a, pwm_1b, pwm_2a, pwm_2b, step_info[4], 1023, 1023);
        }
        else
        {
          millis_step_ini = millis();
          step_info[0] = 2;
        }
      }
      else if (step_info[0] == 2)
      {
        if (millis() < (millis_step_ini + (u_long)step_info[3] * 10))
        {
          mov_control_pid(pwm_1a, pwm_1b, pwm_2a, pwm_2b, step_info[4], 0, 0);
        }
        else
        {
          millis_step_ini = millis();
          step_info[0] = 1;
          ++step_info[5];
        }
      }
    }
  }
  else if (flag_ini == 0)
  {
    Input_1 = 0;
    Input_2 = 0;
    stop_mov();
  }
}
