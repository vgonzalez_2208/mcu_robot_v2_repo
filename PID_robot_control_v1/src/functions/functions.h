#include <Arduino.h>

// Puertos de PWM
#define pwm_1a 7 // Rueda izquierda
#define pwm_1b 8 // Rueda izquierda
#define pwm_2a 9 // Rueda derecha
#define pwm_2b 10 // Rueda derecha

// Puertos de entrada de los encoder
#define ENC_1 1
#define ENC_2 0

typedef unsigned long u_long;
typedef volatile unsigned int vol_u_int;
typedef unsigned int u_int;

// u_long delta_t1 = 0, delta_t2 = 0, start_time = 0;

void print_info(int tipo, u_long delta_t1, u_long delta_t2, u_long start_time, vol_u_int contador_enc1, vol_u_int contador_enc2);
void inicio_mov(int tipo, u_int pwm_val); // Tipo 1: adelante; tipo 2: atrás; tipo 3: rotar izq; tipo 4: rodar der.
void stop_mov();

// Generar info por rueda
void gen_info_test(vol_u_int &cont_enc, vol_u_int &cont_ant, u_long &millis_enc, u_long &delta_t, u_long &start_time, byte (&m)[4]);