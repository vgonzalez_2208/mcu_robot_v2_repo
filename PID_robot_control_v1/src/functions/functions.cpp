#include <functions/functions.h>

void print_info(u_long delta_t, u_long start_time, vol_u_int contador_enc) // Tipo 1: info lado izquierdo; tipo 2: info lado derecho.
{
  // Tiempo entre pulsos 1
  Serial.print(delta_t);
  Serial.print(",");
  Serial.print(millis() - start_time);
  Serial.print(",");
  // Contador de pulsos 1
  Serial.println(contador_enc);
}

void inicio_mov(int tipo, u_int pwm_val) // Tipo 1: adelante; tipo 2: atrás; tipo 3: rotar izq; tipo 4: rodar der.
{
  // Rueda izquierda
  if (tipo == 1 || tipo == 4)
  {
    analogWrite(pwm_1a, 0);       // Movimiento hacia atrás HIGH
    analogWrite(pwm_1b, pwm_val); // Movimiento hacia adelante HIGH
  }
  else
  {
    analogWrite(pwm_1a, pwm_val); // Movimiento hacia atrás HIGH
    analogWrite(pwm_1b, 0);       // Movimiento hacia adelante HIGH
  }
  // Rueda derecha
  if (tipo == 1 || tipo == 3)
  {
    analogWrite(pwm_2a, pwm_val); // Movimiento hacia adelante HIGH
    analogWrite(pwm_2b, 0);       // Movimiento hacia atrás HIGH
  }
  else
  {
    analogWrite(pwm_2a, 0);       // Movimiento hacia adelante HIGH
    analogWrite(pwm_2b, pwm_val); // Movimiento hacia atrás HIGH
  }

  // if (tipo == 1) // Adelante
  // {
  //   // Rueda izquierda
  //   analogWrite(pwm_1a, 0);       // Movimiento hacia atrás HIGH
  //   analogWrite(pwm_1b, pwm_val); // Movimiento hacia adelante HIGH
  //   // Rueda derecha
  //   analogWrite(pwm_2a, pwm_val); // Movimiento hacia adelante HIGH
  //   analogWrite(pwm_2b, 0);       // Movimiento hacia atrás HIGH
  // }
  // else if (tipo == 2) // Atrás
  // {
  //   // Rueda izquierda
  //   analogWrite(pwm_1a, pwm_val); // Movimiento hacia atrás HIGH
  //   analogWrite(pwm_1b, 0);       // Movimiento hacia adelante HIGH
  //   // Rueda derecha
  //   analogWrite(pwm_2a, 0);       // Movimiento hacia adelante HIGH
  //   analogWrite(pwm_2b, pwm_val); // Movimiento hacia atrás HIGH
  // }
  // else if (tipo == 3) // Rotar izq.
  // {
  //   // Rueda izquierda
  //   analogWrite(pwm_1a, pwm_val); // Movimiento hacia atrás HIGH
  //   analogWrite(pwm_1b, 0);       // Movimiento hacia adelante HIGH
  //   // Rueda derecha
  //   analogWrite(pwm_2a, pwm_val); // Movimiento hacia adelante HIGH
  //   analogWrite(pwm_2b, 0);       // Movimiento hacia atrás HIGH
  // }
  // else if (tipo == 4) // Rotar der.
  // {
  //   // Rueda izquierda
  //   analogWrite(pwm_1a, 0);       // Movimiento hacia atrás HIGH
  //   analogWrite(pwm_1b, pwm_val); // Movimiento hacia adelante HIGH
  //   // Rueda derecha
  //   analogWrite(pwm_2a, 0);       // Movimiento hacia adelante HIGH
  //   analogWrite(pwm_2b, pwm_val); // Movimiento hacia atrás HIGH
  // }
}

void stop_mov()
{
  analogWrite(pwm_1a, 0);
  analogWrite(pwm_1b, 0);
  analogWrite(pwm_2a, 0);
  analogWrite(pwm_2b, 0);
}

void gen_info_test(vol_u_int &cont_enc, vol_u_int &cont_ant, u_long &millis_enc, u_long &delta_t, u_long &start_time, byte (&m)[4])
{
  if (cont_enc != cont_ant)
  {
    if (millis_enc == 0)
    {
      delta_t = millis() - start_time;
    }
    else
    {
      delta_t = millis() - millis_enc;
    }
    cont_ant = cont_enc;
    if (cont_enc == 1)
    {
      Serial.println("####Info rueda izquierda####");
      // start_time = 2;
    }
    if (delta_t == 0)
    {
      print_info(delta_t, start_time, cont_enc);
      Serial.println(millis());
      Serial.println();
    }

    // m[1] = delta_t & 0xff;
    m[0] = delta_t;
    m[1] = (cont_enc >> 8) & 0xff;
    m[2] = cont_enc & 0xff;
    millis_enc = millis();
    if (m[0] == m[1])
    {
      Serial.println(delta_t);
    }

    // m[4] = 2;
  }
}